FROM python:3.10
COPY . /macocha
RUN pip install --upgrade pip && pip install -r /macocha/requirements.txt
WORKDIR /macocha
CMD /macocha/macocha.py
