#!/usr/bin/env python3
"""Sample dataset downloader and renderer with S3 bucket storage.

When you run this module, it will download dataset from sample API and save it
to S3 bucket. Afterward it will download all previously saved datasets from S3
bucket and render index.html with sample datasets data. The rendered index.html
is saved to public S3 Bucket. S3 Bucket credentials are read from
~/.aws/credentials.

This module is supposed to be run on schedule, e.g. every hour or day.

The sample data is wheather at Macocha Abyss in Czech Republic.

"""
import argparse
import datetime
import json
import urllib.request
import uuid
from functools import cached_property
from pathlib import Path
from typing import List

import boto3
import pytz
from boto3.resources.base import ServiceResource
from botocore.exceptions import ClientError
from jinja2 import Environment, FileSystemLoader, select_autoescape


class S3Bucket:
    """S3 Bucket for storing generic files."""

    def __init__(self, s3resource: ServiceResource, domain: str, region_name: str = ""):
        self.s3resource = s3resource
        self.domain = domain
        self.current_region = region_name or boto3.session.Session().region_name
        self.bucket_name = domain
        self.tmp_dir = "/tmp"

    @cached_property
    def bucket(self) -> ServiceResource:
        """S3 Resource Bucket object."""
        return self.s3resource.Bucket(self.bucket_name)

    def upload_file(self, filename: Path, content_type: str, acl: str = "private"):
        """Upload *filename* to S3 Bucket."""
        obj = self.bucket.Object(filename.name)
        obj.upload_file(
            str(filename), ExtraArgs={"ContentType": content_type, "ACL": acl}
        )


class DatasetS3Bucket(S3Bucket):
    """S3 Bucket for storing datasets."""

    def __init__(self, s3resource: ServiceResource, domain: str, region_name: str = ""):
        super().__init__(s3resource, domain, region_name)
        self.bucket_name = f"dataset.{domain}"
        if not self._exists():
            self._create()

    def _exists(self) -> bool:
        try:
            self.s3resource.meta.client.head_bucket(Bucket=self.bucket_name)
        except ClientError:
            return False
        return True

    def _create(self) -> None:
        self.s3resource.meta.client.create_bucket(
            Bucket=self.bucket_name,
            CreateBucketConfiguration={"LocationConstraint": self.current_region},
        )

    @cached_property
    def jinja_env(self) -> Environment:
        """Jinja2 Environment object."""
        env = Environment(loader=FileSystemLoader("."), autoescape=select_autoescape())
        return env

    def fetch_dataset(self, api_key: str) -> Path:
        """Fetch new dataset from API, save it to unique file and return filename."""
        # This is sample API url for weather at Macocha Abyss.
        api_url = (
            "https://api.openweathermap.org/data/2.5/weather?"
            f"lat=49.3731&lon=16.7294&exclude=hourly,daily&appid={api_key}"
        )
        # See https://aws.amazon.com/blogs/aws/amazon-s3-performance-tips-tricks\
        # -seattle-hiring-event/ for random filename explanation.
        random_file = Path(self.tmp_dir, str(uuid.uuid4().hex))
        with (
            urllib.request.urlopen(api_url) as fin,
            random_file.open("w", encoding="utf-8") as fout,
        ):
            fout.write(fin.read().decode("utf-8"))
        return random_file

    def download_datasets(self) -> List[Path]:
        """Download datasets from S3 Bucket and returns list with local filenames."""
        paths = []
        for obj in self.bucket.objects.all():
            dest_filename = Path(self.tmp_dir, obj.key)
            if not dest_filename.exists():
                obj.Object().download_file(str(dest_filename))
            paths.append(dest_filename)
        return paths

    def render_index(self, datasets: List[Path]) -> Path:
        """Create new index.html with table extracted from datasets."""
        table = {}
        # TODO: choose timezone depending on region.
        tzone = pytz.timezone("Europe/Prague")
        for dataset in datasets:
            with dataset.open("r", encoding="utf-8") as fin:
                data = json.load(fin)
                timestamp = datetime.datetime.fromtimestamp(data.get("dt"))
                loc_timestamp = timestamp.astimezone(tzone)
                main = data.get("main", {})
                wind = data.get("wind", {})
                weather = data.get("weather", [{}])[0]
                table[dataset.name] = {
                    "timestamp": loc_timestamp,
                    "description": weather.get("description"),
                    "temp_k": main.get("temp"),
                    "temp_c": main.get("temp") - 273.15,
                    "pressure": main.get("pressure"),
                    "humidity": main.get("humidity"),
                    "wind_speed": wind.get("speed"),
                }
        now = datetime.datetime.now(tzone)
        template = self.jinja_env.get_template("index.html.j2")
        index_file = Path(self.tmp_dir, "index.html")
        with index_file.open("w", encoding="utf-8") as fout:
            fout.write(template.render(table=table, now=now))
        return index_file


def parse_commandline() -> argparse.Namespace:
    """Parse arguments on command line and return namespace object."""
    parser = argparse.ArgumentParser(description="Macocha Downloader")
    parser.add_argument("--domain", help="registered domain name", required=True)
    parser.add_argument("--api-key", help="sample API key", default="")
    args = parser.parse_args()
    return args


def main() -> None:
    """Main entry point."""
    args = parse_commandline()
    s3resource = boto3.resource("s3")
    # Prepare datasets.
    dataset_bucket = DatasetS3Bucket(s3resource, args.domain)
    if args.api_key:
        try:
            new_dataset = dataset_bucket.fetch_dataset(args.api_key)
        except urllib.error.HTTPError as error:
            print(f"Cannot download fresh dataset from sample API: {error}")
        else:
            dataset_bucket.upload_file(new_dataset, content_type="application/json")
    all_datasets = dataset_bucket.download_datasets()
    # Generate index.html from prepared datasets.
    new_index = dataset_bucket.render_index(all_datasets)
    public_bucket = S3Bucket(s3resource, args.domain)
    public_bucket.upload_file(new_index, content_type="text/html", acl="public-read")


if __name__ == "__main__":
    main()
