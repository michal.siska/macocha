---
- name: Provision AWS
  hosts: localhost
  gather_facts: false
  connection: local
  tasks:
    - include_vars: vars.yaml

    - name: CloudFormation Stack
      amazon.aws.cloudformation:
        stack_name: "{{ stack_name }}"
        region: "{{ region }}"
        disable_rollback: true
        state: present
        template: "cloudformation.yaml"
        template_parameters:
          KeyName: "{{ keypair }}"
          InstanceType: "{{ instance_type }}"
          SSHLocation: "{{ ssh_location }}"
          RootDomainName: "{{ root_domain_name }}"
        tags:
          stack: "{{ stack_name }}"

    - name: Get EC2 instances info
      amazon.aws.ec2_instance_info:
        region: "{{ region }}"
        filters:
          "tag:stack": "{{ stack_name }}"
          instance-state-name: [ "running" ]
      register: ec2_list

    - name: Add EC2 instances to inventory
      add_host:
        name: "{{ item.public_dns_name }}"
        ansible_user: ec2-user
        host_key_checking: false
        groups: "aws,{{ item.tags.env }},{{ item.tags.app }}"
      no_log: true
      when: ec2_list.instances|length > 0
      loop: "{{ ec2_list['instances'] | flatten(levels=1) }}"

    - name: Basic Website files on S3 Bucket
      amazon.aws.aws_s3:
        region: "{{ region }}"
        bucket: "{{ root_domain_name }}"
        object: "/{{ item }}"
        permission: public-read
        content: '<html><head><title>Macocha</title></head><body><h1>File not found</h1></body></html>'
        metadata: 'Content-Type=text/html'
        encrypt: no
        mode: put
        overwrite: never
      loop:
        - index.html
        - error.html

- name: Wait for instances connection
  hosts: aws
  gather_facts: false
  vars:
    ansible_ssh_common_args: "-o StrictHostKeyChecking=no"
  tasks:
    - name: Wait for instances to become available
      wait_for_connection:

    - name: Gather facts for first time
      setup:

- name: Setup instances
  hosts: downloader
  user: ec2-user
  become: yes
  become_user: root
  gather_facts: false
  tasks:
    - include_vars: vars.yaml

    - name: Docker is installed
      ansible.builtin.package:
        name: docker
        state: present

    - name: Docker service is running
      ansible.builtin.service:
        name: docker
        state: started
        enabled: yes

    - name: Directory for AWS configuration
      ansible.builtin.file:
        path: /root/.aws
        state: directory
        owner: root
        group: root
        mode: '0755'

    - name: AWS credentials for accessing S3 Full Access
      ansible.builtin.template:
        src: credentials.j2
        dest: /root/.aws/credentials
        owner: root
        group: root
        mode: '0600'

    - name: AWS credentials for accessing S3 Full Access
      ansible.builtin.template:
        src: config.j2
        dest: /root/.aws/config
        owner: root
        group: root
        mode: '0600'

    - name: Directory for Docker configuration
      ansible.builtin.file:
        path: /root/.docker
        state: directory
        owner: root
        group: root
        mode: '0700'

    - name: Docker configuration with credentials
      ansible.builtin.template:
        src: config.json.j2
        dest: /root/.docker/config.json
        owner: root
        group: root
        mode: '0600'

    - name: Macocha is running every hour
      ansible.builtin.cron:
        name: "run macocha docker every hour"
        minute: "20"
        job: >
          docker run --rm -v /root/.aws:/root/.aws
          {{ docker_registry_name}}/{{ docker_image_name }}
          /macocha/macocha.py
          --domain {{ root_domain_name }} --api-key {{ api_key }}

    - name: Clean dangling images every day
      ansible.builtin.cron:
        name: "run docker clean every day"
        minute: "50"
        hour: "0"
        job: 'docker rmi $(sudo docker images -f "dangling=true" -q)'
