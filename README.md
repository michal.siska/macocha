# Macocha

## Description
Update 2022-07-28: EC2 instance was shutdown due to end of AWS free tier offer.

I have created this project to learn how to automatically deploy dockerized
application from GitLab CI/CD to AWS (EC2, S3).

The application itself just downloads weather data for Macocha Abyss in Czech
Republic. It uses API from https://openweathermap.org/current. Current schedule
for download is every hour. The downloaded data are stored in private AWS S3 Bucket.
Also it renders index.html with some extracted data and stores this file in
public S3 bucket. You can see result at http://macocha.link/

## Installation

Prerequisite is an AWS account, GitLab account and Openweathermap account. Also
I registered domain macocha.link via Amazon Route 53.

### What cannot be automated

You cannot automate creation and handling with credentials, keys and passwords.

I have created 3 IAM users on AWS:

- *ansible* for running ansible-playbook from my workstation
- *gitlab* for allowing GitLab to deploy directly to AWS EC2
- *macocha* just for the application itself to access AWS S3

I have also created SSH key for password-less access to EC2 instance from my workstation.

```
aws ec2 create-key-pair --key-name my-key-pair --query 'KeyMaterial' --output text > my-key-pair.pem
chmod 400 my-key-pair.pem
```

Credentials for AWS can be saved using AWS CLI. I did this on my workstation and save *ansible* credentials.

```
aws configure
```

GitLab must have configured CI/CD Variables in project settings.

- `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` with *gitlab* credentials
- `AWS_DEFAULT_REGION` set to my region *eu-central-1*
- `AWS_EC2_INSTANCE_ID` set to EC2 instance (it will be created with ansible playbook)

Finally there must be Openweathermap API key, AWS credentials and GitLab Docker
Registry credential on EC2 instance. I saved those as ansible playbook variables
using ansible vault.

```
ansible-vault encrypt_string --vault-password-file vault_password 'redacted_secret' --name 'api_id'
ansible-vault encrypt_string --vault-password-file vault_password 'redacted_secret' --name 'aws_access_key_id'
ansible-vault encrypt_string --vault-password-file vault_password 'redacted_secret' --name 'aws_secret_access_key'
ansible-vault encrypt_string --vault-password-file vault_password 'redacted_secret' --name 'docker_config_auth'
```

The `vault_password` file contains my own password used for AES256 encryption.
Output of `ansible-vault` is saved to `vars.yaml`.

### Automatic provisioning with Ansible

This step will provision AWS resources using CloudFormation template. It will
create one EC2 instance with Security Group, two public S3 buckets and DNS
settings. When EC2 instance is up and runninng the playbook will continue with
configuring the instance. It means install and start Docker, create credential
files and setup crontab for running our dockerized application.

Note: You need to provide ansible with SSH key and with password file to decrypt
encrypted secrets.

```
ansible-playbook --private-key my-key-pair.pem --vault-password-file vault_password playbook.yaml
```

### Automatic integration and deployment with GitLab

Every commit runs pipeline with build stage which creates Docker image and
pushes it to GitLab Docker Registry. Image name contains branch name.  Merge to
main branch will run deploy stage which connects to EC2 instance via
AWS CLI and pulls the image (you will need to enable AWS System Manager
otherwise `aws ssm` for remote execution will not work)

## Further considerations

This project is using Amazon EC2 instance while it would be more suitable to use
Amazon ECS (Elastic Container Service) or even Amazon EKS (Elastic Kubernetes
Service).  The EC2 is quite low in abstraction and you need to manually handle
the whole Docker infrastructure. In my opinion this solution is not
production-grade for dockerized workload but it's still great learning
excercise.

Another missing feature is testing. I should add testing stage to GitLab CI
which at least run some static analyzers aka linters on my code.

Finally there are two bonus points to be implemented:
- Make the application highly available
- Replace simple docker with a container orchestrator

## License
MIT

